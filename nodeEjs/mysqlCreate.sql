CREATE DATABASE IF NOT EXISTS homeworkdb;

CREATE TABLE `homeworkdb`.`Notes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `content` VARCHAR(125) NULL,
  `src` VARCHAR(150) NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `homeworkdb`.`member` (
  `memid` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `tel` VARCHAR(45) NULL,
  PRIMARY KEY (`memid`));