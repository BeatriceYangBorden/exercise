var express = require('express');
var router = express.Router();

var insertQuery = function(data) {
    return new Promise(function(resolve, reject) {
        req = data;

        console.log(req.body.memid);
        console.log(req.body.name);
        console.log(req.body.tel);

        var db = req.con;
        var sql = {
            memid: req.body.memid,
            name: req.body.name,
            tel: req.body.tel
        };
        var qur = db.query('INSERT INTO member(memid, name, tel) Value(?, ?, ?)', [req.body.memid, req.body.name, req.body.tel], function(err, rows) {
            var string;
            if (err) {
                console.log(err);
                return reject(err);
            }
            else
            {
                string = '{"state":"ok"}';
                return resolve(string);
            }
        });
    });
};

var updateQuery = function(data) {
    return new Promise(function(resolve, reject) {
        req = data;

        console.log(req.body.memid);
        console.log(req.body.name);
        console.log(req.body.tel);

        var db = req.con;

        var memid = req.body.memid;

        var sql = {
            name: req.body.update.name,
            tel: req.body.update.tel
        };
            var qur = db.query('UPDATE member SET ? WHERE memid = ?', [sql, memid], function(err, rows) {

            if (err) {
                console.log(err);
                return reject(err);
             }
            else
            {
                return resolve(req.body.memid);
            }
        });
    });
};

var dbQuery = function(req) {
    return new Promise(function(resolve, reject) {
        var db = req.con;
        var filter = "";
        if (req.body.memid) {
            filter = 'WHERE memid = ?';
        }
        console.log(req.body.memid);
        console.log(filter);
        db.query('SELECT * FROM member ' + filter, req.body.memid, function(err, rows) {
            var string;
            if (err) {
                    console.log('dbQuery err:' + err);
                    string = '{"err":"' + err + '"}';
                    return reject(string);
            }
            else
            {
                var data = rows;
                
                /*
                string = '['; 
                data.forEach(function(element, index) {
                    if (index != 0)
                        string += ',';
                    string += '{"memid":"' + element.memid + '","name":"' + element.name + '","tel":"' + element.tel + '"}';
                }); 
                string = ']'; 
                */

                if (!rows.length)
                    string = '{}';
                else
                    string = '{"memid":"' + data[0].memid + '","name":"' + data[0].name + '","tel":"' + data[0].tel + '"}';
               
                obj = string;
                console.log(obj);
                return resolve(obj);
            }
        });
    });
};

var deleteQuery = function(data) {
    return new Promise(function(resolve, reject) {
        req = data;

        console.log(req.body.memid);

        var db = req.con;
        
        var qur = db.query('DELETE FROM member WHERE memid = ?', req.body.memid, function(err, rows) {
            if (err) {
                console.log(err);
                return reject(err);
            }
            else
            {
                return resolve(req.body.memid);
            }
        });
    });
};

var setRedis = function(req, data) {
    return new Promise(function(resolve, reject) {
        var jsonObj = JSON.parse(data);
            console.log(jsonObj.memid);
            console.log(jsonObj.name);
            console.log(jsonObj.tel);
            req.redis.hmset(jsonObj.memid, {
                memid: jsonObj.memid,
                name: jsonObj.name,
                tel: jsonObj.tel
            });
            req.redis.expire(jsonObj.memid, 60);
        return resolve(jsonObj);
    });
};

var delRedis = function(req, key){
    return new Promise(function(resolve, reject) {
        console.log('delRedis memid  :' + key);
        if (key == null)
            return resolve(key);
        else
        {
            req.redis.del(key, function (err, res) {
                if(err){
                    console.log(err);
                    return reject(err);
                }
                else
                {
                    console.log('delRedis OK');
                    return resolve(key);
                }
            });
        }
    });
}

var existsRedis = function(req, key){
    return new Promise(function(resolve, reject) {
        console.log('existsRedis memid :' + key);
        req.redis.exists(key, function (err, exists) {
            if(err){
                console.log(err);
                return reject(err);
            }
            else
            {
                console.log('existsRedis = ' + exists);
                return resolve(exists);
            }
        });
    });
}

router.get('/', function(req, res, next) {

    /* var db = req.con;
    var data = '';

    var id = '';
    var id = req.query.id;

    var filter = "";
    if (id) {
        filter = 'WHERE id LIKE ? OR name LIKE ? OR content LIKE ?';
    }
    console.log(filter);
    db.query('SELECT * FROM member ' + filter, [ '%' + id + '%', '%' + id + '%', '%' + id + '%'], function(err, rows) {
        if (err) {
            console.log(err);
        }
        var data = rows;

        // use notes.ejs 
        res.render('homework', { 
            title: 'Welcome HomeWork', 
            data: data, 
            id: id });
    }); */

});

router.post('/insert', function(req, res, next) {
    
    
 
    console.log('hello insert');
    insertQuery(req)
        .then(function(result) {
            console.log('Result: ', result);
            res.setHeader('Content-Type', 'application/json');
            res.send(result);
        })
        .catch(function (err) {
            console.log('Error: ', err);
            res.setHeader('Content-Type', 'application/json');
            res.send('{"err":"' + err + '"}');
        });
});

router.post('/update', function(req, res, next) {
    
    console.log('hello update');
    console.log(req.body.memid);
    console.log(req.body.update.name);
    console.log(req.body.update.tel);
    updateQuery(req)
        .then(function(result) {
            console.log('Result: ', result);
            return existsRedis(req, req.body.memid);
        })
        .then(function(result) {
            if (result == 0)
            {
                result = null;
                return result;
            }    
            else
            {
                console.log('Result: ', result);
                return delRedis(req, req.body.memid);
            }
        })
        .then(function(result) {
            var string = '{"state":"ok", "memid":"' + result + '"}';
            console.log('Result: ', string);
            res.setHeader('Content-Type', 'application/json');
            res.send(string);
        })
        .catch(function (err) {
            console.log('Error: ', err);
            res.setHeader('Content-Type', 'application/json');
            res.send('{"err":"' + err + '"}');
        });
});

router.post('/query', function(req, res, next) {

    console.log('hello query');
    console.log(req.body.memid);

    var memid = req.body.memid;
    var resultObj;

    req.redis.hgetall(memid)
        .then(function(hgetallResponse) {
            const helpSessionData = JSON.stringify(hgetallResponse);
            return new Promise(function(resolve, reject) {
                console.log('helpSessionData = ' + helpSessionData);
                if (memid == null)
                    return reject('Please input memid');
                else
                    return resolve(helpSessionData);
            })
        .then(function(result) {
            console.log('Result1: ', result);
            if (result != '{}') {
                resultObj = JSON.parse(result);
                console.log('resultObj: ', resultObj);
                return null;
            }
            else
                return memid;
        })
        .then(function(result) {
            console.log('Result2: ', result);
            if (result == null)  return null;
            return dbQuery(req);
        })
        .then(function(result) {
            console.log('Result3: ', result);
            if (result == '{}') //db not found
            {
                return result;
            }
            else if (result != null && result != '[]')
            {
                return setRedis(req, result);
            }
            else
                return null;  
        })
        .then(function(result) {
            console.log('Result4: ', result);
            if (result == '{}') //db not found
                resultObj = '{"state":"db not found"}'
            else if (result != null)
                resultObj = result;
            
            console.log('resultObj: ', resultObj);
            res.setHeader('Content-Type', 'application/json');
            res.send(resultObj);
        })
        .catch(function (err) {
            console.log('Error: ', err);
            res.setHeader('Content-Type', 'application/json');
            res.send('{"err":"' + err + '"}');
        });
    });
    
});

router.post('/delete', function(req, res, next) {
    
    console.log('hello delete ');
    console.log(req.body.memid);
    deleteQuery(req)
        .then(function(result) {
            console.log('Result: ', result);
            return existsRedis(req, result);
        })
        .then(function(result) {
            if (result == 0)
            {
                result = null;
                return result;
            }    
            else
            {
                console.log('Result: ', req.body.memid);
                return delRedis(req, req.body.memid);
            }
        })
        .then(function(result) {
            var string = '{"state":"ok", "memid":"' + result + '"}';
            console.log('Result: ', string);
            res.setHeader('Content-Type', 'application/json');
            res.send(string);
        })
        .catch(function (err) {
            console.log('Error: ', err);
            res.setHeader('Content-Type', 'application/json');
            res.send('{"err":"' + err + '"}');
        });
});

module.exports = router;