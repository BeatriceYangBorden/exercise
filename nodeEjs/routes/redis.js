var express = require('express');
var router = express.Router();



router.get('/', function(req, res, next) {
    /* req.redis.set('foo', 'bar');
    req.redis.get('foo', function (err, result) {
        console.log(result);
    });
    
    // Or using a promise if the last argument isn't a function
    req.redis.get('foo').then(function (result) {
        console.log(result);
    });
    
    // Arguments to commands are flattened, so the following are the same:
    req.redis.sadd('set', 1, 3, 5, 7);
    req.redis.sadd('set', [1, 3, 5, 7]);
    
    // All arguments are passed directly to the redis server:
    req.redis.set('key', 100, 'EX', 10); //EXPIRE key 10 : Timoout
    req.redis.set('key', 100, 'EX', 10); */
    
     /* req.redis.hmset(1, {
        memid:1,
        name: 'Beatrice',
        tel: '0958598678'
    });
    req.redis.hmset(2, {
        memid:2,
        name: 'Beatrice2',
        tel: '09585986782'
    });
    req.redis.hmset(3, {
        memid:3,
        name: 'Beatrice3',
        tel: '09585986783'
    }); */

    //req.redis.expire('member', 10);

    /* req.redis.hgetall('1', function (value) {
        console.log(value);
    }); */

    /* req.redis.hgetall('5')
        .then((hgetallResponse) => {
            const helpSessionData = JSON.stringify(hgetallResponse);
            return new Promise(function(resolve, reject) {
                console.log(helpSessionData);
                 if (helpSessionData != '{}') {
                    return resolve(helpSessionData);
                 } else {
                    return reject('Not Found');
                }
            })
        .then(function(result) {
            console.log('Result: ', result);
        })
        .catch(function (err) {
            console.log('Result Error: ', err);
        });
    }); */
        

    res.render('redis', { title: 'Hello ioredis' });
});
module.exports = router;