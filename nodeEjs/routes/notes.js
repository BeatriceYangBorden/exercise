var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {

    var db = req.con;
    var data = '';

    var id = '';
    var id = req.query.id;

    var filter = "";
    if (id) {
        filter = 'WHERE id LIKE ? OR name LIKE ? OR content LIKE ?';
    }
    console.log(filter);
    db.query('SELECT * FROM Notes ' + filter, [ '%' + id + '%', '%' + id + '%', '%' + id + '%'], function(err, rows) {
        if (err) {
            console.log(err);
        }
        var data = rows;

        // use notes.ejs 
        res.render('notes', { 
            title: 'Welcome', 
            data: data, 
            id: id });
    });

});

router.get('/userDelete', function(req, res, next) {

    var id = req.query.id;
    var db = req.con;
    console.log('userDelete');
    var qur = db.query('DELETE FROM Notes WHERE id = ?', id, function(err, rows) {
        if (err) {
            console.log(err);
        }
        res.redirect('/notes');
    });
});

router.get('/userAdd', function(req, res, next) {

    var db = req.con;

    var qur = db.query('INSERT INTO Notes(name, content, src) Value(?, ?, ?)', [req.query.name, req.query.content, req.query.src], function(err, rows) {
        if (err) {
            console.log(err);
        }
        res.redirect('/notes');
    });
});

// edit page
router.get('/userEdit', function(req, res, next) {

    var id = req.query.id;
    //console.log(id);

    var db = req.con;
    var data = "";

    db.query('SELECT * FROM Notes WHERE id = ?', id, function(err, rows) {
        if (err) {
            console.log(err);
        }

        var data = rows;
        res.render('userEdit', { title: 'Edit Account', data: data });
    });

});

router.post('/userEdit', function(req, res, next) {
    
    var db = req.con;

    var id = req.body.id;

    var sql = {
        name: req.body.name,
        content: req.body.content,
        src: req.body.src
    };

    var qur = db.query('UPDATE Notes SET ? WHERE id = ?', [sql, id], function(err, rows) {
        if (err) {
            console.log(err);
        }

        res.setHeader('Content-Type', 'application/json');
        res.redirect('/notes');
    });


});



module.exports = router;